<?php

namespace Drupal\remote_entity_creation_client\Plugin\WebformHandler;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Serialization\Yaml;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\webformSubmissionInterface;

/**
 * Download a Webform handler.
 *
 * @WebformHandler(
 *   id = "webform_remote_push",
 *   label = @Translation("Webform remote push"),
 *   category = @Translation(" CMS sync "),
 *   description = @Translation(" Webform handler access  "),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_REQUIRED,
 * )
 */
class WebformRemotePushFormHandler extends WebformHandlerBase {
  /**
	 *
	 * @return string
	 */
	protected function testUrl() {

		return '';
	}

	/**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {

		$testUrl = false;
		$client = \Drupal::httpClient();
		$values = $webform_submission->getData();

		if ($testUrl === false) {
			\Drupal::logger('webform_remote_push')->error('Submit handler testing is on');
			$response = $client->request('GET', $this->testUrl(), []);
		}
		//$post_url = $this->configuration['submission_url'];
   
		$code = $response->getStatusCode();
		if ($code >= 400 || $code === 0) {
			\Drupal::logger('webform_remote_push')->error('Status code : @error', ['@error' => 'Sending to third party did not work' . print_r($values, 1)]);
		}

		return true;
	}

	/**
	 * {@inheritdoc}
	 */
	public function confirmForm(array &$form, FormStateInterface
  $form_state, WebformSubmissionInterface $webform_submission) {
		//  $form_id = 'webform_submission_' . $webform_submission->getWebform()->id() . '_form';
    //  if ($form_id == 'webform_submission_xxx_form') {
    //    if (based_on_some_condition) {
    /* Here 12 is the node id where i wanted to redirect to */
   //     $form_state->setRedirect('entity.node.canonical', ['node' => 12]);
    //  }
  }
}
