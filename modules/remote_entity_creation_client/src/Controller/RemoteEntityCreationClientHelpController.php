<?php

/**
 * @file
 * Contains \Drupal\remote_entity_creation\Controller\RemoteEntityCreationHelpController
 */

namespace Drupal\remote_entity_creation_client\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Controller routines for help page
 */
class RemoteEntityCreationClientHelpController extends ControllerBase {

	/**
	 * Constructs a the help page.
	 *
	 * The router _controller callback, maps the path
	 * '/admin/config/remote-entity-creation-client' to this method.
	 *
	 */
	public function index() {

		$output = '<h2>' . $this->t('Remote entiity creation client') . '</h2>';
		$output .= '<table"><tr><td>';


		$output .= '<p>' . $this->t('The module caputures the inputs from a form varaible and on submit will send it two a third party '
				. 'service. Currently it caputes the data from any form that has the web hook submit handler') . '</p>';

		$output .= '<h3>' . $this->t('Module road map') . '</h3>';
		$output .= '<ol>';
		$output .= '<li><s>Add form for Api key for server authentication</s></li>';
		$output .= '<li>Get entity variable mapping completed</li>';
		$output .= '<li>Conplete webform integration</li>';
		$output .= '<li>Test guzzle</li>';
		$output .= '<li>Map domain against form id(This could be done with features not sure yet)</li>';
		$output .= '</ol>';


		$output .= '</td>';
		$output .= '<td>';


		$output .= '<h3>' . $this->t('@todo currently I am working on url variable mapping') . '</h3>';
		$output .= '<p>' . $this->t('') . '</p>';
		$output .= '<p>' . $this->t('This video explains how to configure this module') . '</p>';
		$output .= '<iframe width="560" height="315" src="https://www.youtube.com/embed/Xs9Z2fMcCrg" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>';
		$output .= '<p>' . $this->t('This is a temp video until module is in beta') . '</p>';


		$output .= '</td></tr></table>';

		return [
			'#markup' => $output,
			'#allowed_tags' => ['iframe', 'h2', 'h3', 'p', 'ol', 'li', 'table', 'td', 'tr', 's'],
		];
	}

}
