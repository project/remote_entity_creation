<?php

namespace Drupal\remote_entity_creation_client\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\EventSubscriber\ResponseGeneratorSubscriber;

/**
 * Class UrlVariables.
 */
class UrlVariablesMapping extends ConfigFormBase {

	/**
	 * @var array
	 */
	protected $formGroupKey = [];

	/**
	 * Drupal\Core\EventSubscriber\ResponseGeneratorSubscriber definition.
	 *
	 * @var \Drupal\Core\EventSubscriber\ResponseGeneratorSubscriber
	 */
	protected $responseGeneratorSubscriber;

	/**
	 * Constructs a new UrlVariables object.
	 */
	public function __construct(
	ConfigFactoryInterface $config_factory, ResponseGeneratorSubscriber $response_generator_subscriber
	) {
		parent::__construct($config_factory);
		$this->responseGeneratorSubscriber = $response_generator_subscriber;
	}

  	/**
	 * {@inheritdoc}
	 */
	public static function create(ContainerInterface $container) {

		return new static(
			$container->get('config.factory'), $container->get('response_generator_subscriber')
		);
	}

	/**
	 * {@inheritdoc}
	 */
	protected function getEditableConfigNames() {

		return [
			'remote_entity_creation_client.urlvariablesmapping',
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function getFormId() {
		return 'url_variables_mapping';
	}

	/**
	 * {@inheritdoc}
	 */
	public function buildForm(array $form, FormStateInterface $form_state) {

		$config = $this->config('remote_entity_creation_client.urlvariablesmapping');
		$rawData = $config->getRawData();

	//\Drupal::configFactory()->getEditable('remote_entity_creation_client.urlvariablesmapping')->delete();
		$response = '@todo this has not been completed';

		$form['url_type'] = [
			'#type' => 'details',
			'#title' => t('Url pattern the request needs to receive'),
			'#description' => t('The suggested url is example.com/' . $response),
			'#open' => TRUE, // Controls the HTML5 'open' attribute. Defaults to FALSE.
		];

		// @todo this needs to be added 
		//	$form['url_type']['variable_format'] = [
		//		'#type' => 'checkbox',
		//		'#title' => t('If checked the system will receive variables in a non drupal way'),
		//		'#default_value' => $config->get('variable_format'),
		//	];



		$form['url_builder'] = [
			'#type' => 'details',
			'#title' => t('Request url variables mapping'),
			'#open' => TRUE, // Controls the HTML5 'open' attribute. Defaults to FALSE.
		];

		$x = 1;
		foreach ($rawData as $key => $value) {
			if ($key != 'variable_format' && $config->get($key) !== '') {
				$form['url_builder'][$key] = [
					'#type' => 'textfield',
						'#title' => $this->t("Variable $x "),
						'#default_value' => $config->get($key),
					];
					$x++;
			}
		}


		// always add other field
		$urlKey = "url_key_" . $x;
		$form['url_builder'][$urlKey] = [
			'#type' => 'textfield',
			'#title' => $this->t("New Variable $x "),
			'#default_value' => '',
		];
		return parent::buildForm($form, $form_state);
	}

	/**
	 * {@inheritdoc}
	 */
	public function validateForm(array &$form, FormStateInterface $form_state) {

		parent::validateForm($form, $form_state);
	}

	/**
	 * {@inheritdoc}
	 */
	public function submitForm(array &$form, FormStateInterface $form_state) {

		$inputArr = $form_state->getUserInput();

		$x = 1;
		foreach ($inputArr as $key => $value) {
			$result = substr(trim($key), 0, 4);
			if ($key != 'variable_format') {
				if (trim($result) == 'url_') {
					$keyValue = 'url_key_' . $x;
					$formValue = $form_state->getValue($keyValue);
					//	if ($formValue == '') {
					//	$this->config('remote_entity_creation_client.urlvariablesmapping')
					//		->set("url_key_$x", $formValue)
					//	->delete();
					//	}
					//	else {
					$this->config('remote_entity_creation_client.urlvariablesmapping')
						->set("url_key_$x", $formValue)
						->save();
				//	}
				}
			}
			$x++;
		}

		$this->config('remote_entity_creation_client.urlvariablesmapping')
			->set('variable_format', $form_state->getValue('variable_format'))
			->save();
		parent::submitForm($form, $form_state);
	}

}
