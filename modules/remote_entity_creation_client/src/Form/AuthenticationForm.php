<?php

namespace Drupal\remote_entity_creation_client\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class AuthenticationForm.
 */
class AuthenticationForm extends ConfigFormBase {

	/**
	 * {@inheritdoc}
	 */
	protected function getEditableConfigNames() {

		return [
			'remote_entity_creation_client.authentication',
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function getFormId() {

		return 'authentication_client_form';
	}

	/**
	 * {@inheritdoc}
	 */
	public function buildForm(array $form, FormStateInterface $form_state) {

		$config = $this->config('remote_entity_creation_client.authentication');
		$form['request_key'] = [
			'#type' => 'textfield',
			'#title' => $this->t('Request key'),
			'#description' => $this->t('Api key created by server these need to match for entity to be created'),
			'#maxlength' => 256,
			'#size' => 64,
			'#default_value' => $config->get('request_key'),
		];
		return parent::buildForm($form, $form_state);
	}

	/**
	 * {@inheritdoc}
	 */
	public function validateForm(array &$form, FormStateInterface $form_state) {

		parent::validateForm($form, $form_state);
	}

	/**
	 * {@inheritdoc}
	 */
	public function submitForm(array &$form, FormStateInterface $form_state) {

		parent::submitForm($form, $form_state);
		$this->config('remote_entity_creation_client.authentication')
			->set('request_key', $form_state->getValue('request_key'))
			->save();
	}

}
