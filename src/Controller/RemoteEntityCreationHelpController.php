<?php

/**
 * @file
 * Contains \Drupal\remote_entity_creation\Controller\RemoteEntityCreationHelpController
 */

namespace Drupal\remote_entity_creation\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Controller routines for help page
 */
class RemoteEntityCreationHelpController extends ControllerBase {

	/**
	 * Constructs a the help page.
	 *
	 * The router _controller callback, maps the path
	 * '/admin/config/remote-entity-creation' to this method.
	 *
	 */
	public function index() {

		$output = '<h2>' . $this->t('Remote entiity creation ') . '</h2>';
		$output .= '<table"><tr><td>';


		$output .= '<p>' . $this->t(''
				. 'This module allows a developer to select an entity type '
				. 'and create data remotely. '
				. 'to do this a request is sent to the following route '
				. '/remote-entity-creation/{key}/{serialised_data}'
				. 'the request is processed and if authenicated the entity is created'
				. 'amd a json response returns a object with a success key') . '</p>';

		$output .= '<h3>' . $this->t('Module road map') . '</h3>';
		$output .= '<ol>';
		$output .= '<li><s>Allow JSON object to set system entity types </s></li>';
		$output .= '<li>Abstract field arrays for entity creation </li>';
		$output .= '<li>Write more tests for the Authenication package</li>';
		$output .= '<li>Setup ip locking from a client request</li>';
		$output .= '<li>Create a new route that does not need entity mapping(this will be for nodes only)</li>';
		$output .= '<li>Set up a custom user on install So that user 1 is not creating entities remotely</li>';
		$output .= '<li>Add form for email entity creation hooks see .module</li>';
		$output .= '</ol>';


		$output .= '</td>';
		$output .= '<td>';


		$output .= '<h3>' . $this->t('@todo currently I am working field arrays being not set manually') . '</h3>';
		$output .= '<p>' . $this->t('') . '</p>';
		$output .= '<p>' . $this->t('This video explains how to configure this module') . '</p>';
		$output .= '<iframe width="560" height="315" src="https://www.youtube.com/embed/Xs9Z2fMcCrg" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>';
		$output .= '<p>' . $this->t('This is a temp video until module is in beta') . '</p>';


		$output .= '</td></tr></table>';

		return [
			'#markup' => $output,
			'#allowed_tags' => ['iframe', 'h2', 'h3', 'p', 'ol', 'li', 'table', 'td', 'tr', 's'],
		];
	}

}
