<?php

/**
 * @file
 * Contains \Drupal\remote_entity_creation\Controller\RemoteEntityCreationController
 */

namespace Drupal\remote_entity_creation\Controller;

// System classes
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

// Authorisation classes
use Drupal\remote_entity_creation\Authentication\KeyAuthentication;
use Drupal\remote_entity_creation\Authentication\DomainAuthentication;

// Entity creation classes
use Drupal\remote_entity_creation\Entity\DataMapping;
use Drupal\remote_entity_creation\Entity\EntityCreation;
use Drupal\remote_entity_creation\Entity\RequestDataParsing;

/**
 * Controller routines for page example routes.
 */
class RemoteEntityCreationController extends ControllerBase {

	/**
	 * This tests to see if the remote request is allowed to
	 * create entity by the following means,
	 * 1. The domain the request came from.
	 * 2. The IP the the request came from @todo
	 * 3. A special request key that is created by this module.
	 *
	 * All authentication methods are setup and  can be accessed at the via the
	 * URL below,
	 * /admin/config/remote-entity-creation/authentication
	 *
	 * @param Request $request
	 * @param type $key
	 * @return boolean
	 */
	protected function accessCorrect(Request $request, $key) {

		$domainObj = new DomainAuthentication();
		$domainObj->setDomainName($request);

		$keyObj = new KeyAuthentication();
		$keyObj->setKey($key);

		if ($domainObj->getDomainAuthentication() === true &&
			$keyObj->getKeyAuthentication() === true
		) {
			return true;
		}
		return false;
	}

	/**
	 * Parses data so that it is in the right format. Then maps the data against
	 * the entity that has been selected field types and then creates a new
	 * entity that is added to the database. This is not published until an
	 * administrator makes that choice
	 *
	 * @param mixed $data
	 */
	protected function createData($data) {
    
		$dataMapping = new DataMapping(new RequestDataParsing($data));
		$obj = new EntityCreation($dataMapping->getDataMappedArray());
		return $obj->getDataCreationResult();
	}

	/**
	 * @param  Request $request
	 * @param  String $key
	 * @param  Json Object $data
	 * @return Response
	 */
	public function add(Request $request, $key, $data) {

	
		$response = new Response();
		if ($this->accessCorrect($request, $key) === true) {
			\Drupal::logger('remote_entity_creation')->error('Log: @error', ['@error' => 'access true' . ' data:  ' . $data]);
			$response->setContent(json_encode(array_merge(['domain.access' => 'true', 'data.sent' => $data], $this->createData($data))));

			//--create entity is next
		}
		else {
			$response->setContent(json_encode(['access' => 'false']));
			\Drupal::logger('remote_entity_creation')->error('Log: @error', ['@error' => 'access false' . ' data:  ' . $data]);
		}

		return $response;
	}

}
