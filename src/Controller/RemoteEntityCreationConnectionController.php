<?php

/**
 * @file
 * Contains \Drupal\remote_entity_creation\Controller\RemoteEntityCreationConnectionController
 */

namespace Drupal\remote_entity_creation\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Drupal\remote_entity_creation\Authentication\DomainAuthentication;

/**
 * @todo when this comes out of alpha the will need to be only seen by administration
 *
 * Controller test route works .
 */
class RemoteEntityCreationConnectionController extends ControllerBase {

	/**
	 * @return Response
	 */
	public function connection(Request $request) {

		$objRequest = new DomainAuthentication();
		$domain = $objRequest->setDomainName($request);

		return [
			'#markup' => '<p>'
			. t('connection made with the following domain: ')
			. $domain
			. '<br/>'
			. '</p>',
			'#title' => $this->t('connection'),
		];
	}

}
