<?php

namespace Drupal\remote_entity_creation\Entity;

use Drupal\remote_entity_creation\Interfaces\RequestDataParsingInterface;

/**
 * He
 * {"title":"this is a remote title","body":"this is the body of the text"}
 * @see Drupal\remote_entity_creation\Interfaces\RequestDataParsingInterface
 */
class RequestDataParsing implements RequestDataParsingInterface {

	/**
	 * @var mixed
	 */
	protected $data;

	/**
	 *
	 * @var array
	 */
	protected $dataPhpArray;

	/**
	 * @param mixed $data
	 */
	public function __construct($data) {

		$this->data = $data;
		$this->dataStuctureType();
	}

	/**
	 * Works out what the data structure type is so that it can be converted into
	 * a PHP array
	 */
	protected function dataStuctureType() {

		if ($this->isJson()) {
			$this->dataPhpArray = (array) json_decode($this->data);
		}
	}

	/**
	 * Test to see if it is a JSON object
	 * @return string
	 */
	protected function isJson() {

		json_decode($this->data);
		return (json_last_error() == JSON_ERROR_NONE);
	}

	/**
	 * {@inheritdoc}
	 */
	public function getDataArray() {

		return $this->dataPhpArray;
	}

}
