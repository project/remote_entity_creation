<?php

namespace Drupal\remote_entity_creation\Entity;

use Drupal\remote_entity_creation\Interfaces\EntityCreationInterface;
use \Drupal\node\Entity\Node;
use Drupal\field\FieldConfigInterface;

/**
 * @see Drupal\remote_entity_creation\Interfaces\EntityCreationInterface
 */
class EntityCreation implements EntityCreationInterface {

	/**
	 * The field names will be the keys and the values will be the data
	 * inserted into the entity type
	 *
	 * @var array
	 */
	protected $entityDataArr;

	/**
	 * @param array $entityDataArr
	 */
	public function __construct(Array $entityDataArr) {
		$this->entityDataArr = $entityDataArr;
		$this->createEntity();
	}

	/**
	 * @todo array could have config type so reduce database calls
	 */
	protected function createEntity() {
		$config = \Drupal::config('remote_entity_creation.entity_mapping');
		if (isset($this->entityDataArr['type'])) {
			$nodeType = $this->entityDataArr['type'];
		}
		else {
			$nodeType = $config->get('type');
		}

		if ($nodeType != '') {
			$node = Node::create([
					'type' => $nodeType,
					'langcode' => 'en',
					'created' => REQUEST_TIME,
					'changed' => REQUEST_TIME,
					'uid' => 1,
					'status' => 0,
					'title' => $this->entityDataArr['title'],
					'body' => [
						'value' => $this->entityDataArr['body'],
						'format' => 'full_html',
					],
			]);
			$node->save();
		}
		//  \Drupal::service('path.alias_storage')->save("/node/" . $node->id(), "/my/path", "en");
	}

	/**
	 * {@inheritdoc}
	 */
	public function getDataCreationResult() {

		return $this->entityDataArr;
	}

}
