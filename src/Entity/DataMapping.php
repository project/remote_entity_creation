<?php

namespace Drupal\remote_entity_creation\Entity;

use Drupal\remote_entity_creation\Interfaces\DataMappingInterface;

/**
 */
class DataMapping implements DataMappingInterface {

	/**
	 * @var array 
 */
	protected $fieldMachineNames;

	/**
	 *
	 * @var array
	 */
	protected $data;

	/**
	 * Keys are entity machine name fields and values are what
	 * need to be added to the entity on creation.
	 *
	 * @var array
	 */
	protected $dataMapping;

	/**
	 * @param Object $data
	 */
	public function __construct(RequestDataParsing $data) {

		$this->contentTypeFields($data);
		$this->data = $data->getDataArray();
		$this->dataMappingCreation($data);
	}

	/**
	 *  * Access the fields of selected entity type so that data mapping can occur
	 *  for t
	 * @param Object $data
	 */
	protected function contentTypeFields($data) {
		$config = \Drupal::config('remote_entity_creation.entity_mapping');
		if (isset($data->getDataArray()['system_type'])) {
			$type = $data->getDataArray()['system_type'];
		}
		else {
			$type = $config->get('type');
		}


		if (trim($type) !== '') {
			$entityManagerResult = \Drupal::entityManager()->getFieldDefinitions('node', $type);
			$this->fieldMachineNames = array_keys($entityManagerResult);
		}
		else {
			\Drupal::logger('remote_entity_creation')->error('Log: @error', ['@error' => 'No entity to be use for remote creation has been selected data can\'t be created']);
			$this->fieldMachineNames[] = t('No entity has been selected please contact website owner');
		}
	}

	/**
	 * Creates a data mapping array that has key values that equals the entity fields
	 * machine names, and a value that needs to be added to that machine name on
	 * entities creation.
	 *
	 *  @param Object $data
	 */
	protected function dataMappingCreation($data) {

		foreach ($this->data as $dataKey => $dataValue) {
			foreach ($this->fieldMachineNames as $machineValue) {
				if ($dataKey == $machineValue) {
					$this->dataMapping[$dataKey] = $dataValue;
				}
			}
		}
		if (count($this->dataMapping) == 0) {
			$this->dataMapping['rec.error'] = t('Data mapping is empty');
		}
		if (isset($data->getDataArray()['system_type'])) {
			$this->dataMapping['type'] = $data->getDataArray()['system_type'];
		}
	}

	/**
	 * {@inheritdoc}
	 */
	public function getDataMappedArray() {

		return $this->dataMapping;
	}

	/**
	 * @return array
	 */
	public function getArrStructures() {
	
		return [
			$this->fieldMachineNames,
			json_encode($this->fieldMachineNames)
		];

	}

}
