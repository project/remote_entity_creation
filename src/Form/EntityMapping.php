<?php

namespace Drupal\remote_entity_creation\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\EventSubscriber\ResponseGeneratorSubscriber;

/**
 * Class Entity Mapping.
 */
class EntityMapping extends ConfigFormBase {

	/**
	 * @var array
	 */
	protected $formGroupKey = [];

	/**
	 * Drupal\Core\EventSubscriber\ResponseGeneratorSubscriber definition.
	 *
	 * @var \Drupal\Core\EventSubscriber\ResponseGeneratorSubscriber
	 */
	protected $responseGeneratorSubscriber;

	/**
	 * Constructs a new UrlVariables object.
	 */
	public function __construct(
	ConfigFactoryInterface $config_factory, ResponseGeneratorSubscriber $response_generator_subscriber
	) {
		parent::__construct($config_factory);
		$this->responseGeneratorSubscriber = $response_generator_subscriber;
	}

	public static function create(ContainerInterface $container) {

		return new static(
			$container->get('config.factory'), $container->get('response_generator_subscriber')
		);
	}

	/**
	 * {@inheritdoc}
	 */
	protected function getEditableConfigNames() {

		return [
			'remote_entity_creation.entity_mapping',
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function getFormId() {

		return 'entity_mapping';
	}

	/**
	 * {@inheritdoc}
	 */
	public function buildForm(array $form, FormStateInterface $form_state) {

		$config = $this->config('remote_entity_creation.entity_mapping');
		$response = $config->get('type');
		$form['entity_mapping'] = [
			'#type' => 'details',
			'#title' => t("Remote entity to be created is currently: $response "),
			'#open' => TRUE, // Controls the HTML5 'open' attribute. Defaults to FALSE.
		];

	//	$form['entity_mapping']['definitions'] = [
//			'#type' => 'select',
		//		'#multiple' => false,
		//		'#size' => 10,
		//		'#title' => '',
//			'#options' => array_keys(\Drupal::entityTypeManager()->getDefinitions()),
		//'#default_value' =>
	//	];

		$entityTypes = [];
		$types = \Drupal::entityManager()->getStorage('node_type')->loadMultiple();
		foreach ($types as $type) {
			$entityTypes[$type->id()] = $type->label();
		}


		$form['entity_mapping']['type'] = [
			'#type' => 'select',
			'#multiple' => false,
			'#size' => 10,
			'#title' => '',
			'#options' => $entityTypes,
		  '#default_value' => $response
		];

		return parent::buildForm($form, $form_state);
	}

	/**
	 * {@inheritdoc}
	 */
	public function validateForm(array &$form, FormStateInterface $form_state) {

		parent::validateForm($form, $form_state);
	}

	/**
	 * {@inheritdoc}
	 */
	public function submitForm(array &$form, FormStateInterface $form_state) {
      //	$inputArr = $form_state->getUserInput();

		$this->config('remote_entity_creation.entity_mapping')
			->set('type', $form_state->getValue('type'))
			->save();

		parent::submitForm($form, $form_state);
	}

}
