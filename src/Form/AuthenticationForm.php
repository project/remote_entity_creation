<?php

namespace Drupal\remote_entity_creation\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class AuthenticationForm.
 */
class AuthenticationForm extends ConfigFormBase {

	/**
	 * {@inheritdoc}
	 */
	protected function getEditableConfigNames() {

		return [
			'remote_entity_creation.authentication',
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function getFormId() {

		return 'authentication_form';
	}

	/**
	 *  Returns authentication key created on install
	 *
	 * @return string
	 */
	protected function viewAuthenticationKey() {

		$key = \Drupal::database()->query('SELECT {special_key} FROM {remote_entity_creation}')->fetchAll();
		return $key[0]->special_key;
	}

	/**
	 * {@inheritdoc}
	 */
	public function buildForm(array $form, FormStateInterface $form_state) {

		$config = $this->config('remote_entity_creation.authentication');
		$form['domain'] = [
			'#type' => 'textfield',
			'#title' => $this->t('Domain'),
			'#description' => $this->t('Domain allowed to submit request'),
			'#maxlength' => 128,
			'#size' => 64,
			'#default_value' => $config->get('domain'),
		];
		$form['request_key'] = [
			'#type' => 'textfield',
			'#title' => $this->t('Request key'),
			'#description' => $this->t('This key is created by the system and needs to be added to the url'),
			'#maxlength' => 256,
			'#size' => 64,
			'#attributes' => ['readonly' => 'readonly'],
			'#default_value' => $this->viewAuthenticationKey(),
		];
		return parent::buildForm($form, $form_state);
	}

	/**
	 * {@inheritdoc}
	 */
	public function validateForm(array &$form, FormStateInterface $form_state) {

		parent::validateForm($form, $form_state);
	}

	/**
	 * {@inheritdoc}
	 */
	public function submitForm(array &$form, FormStateInterface $form_state) {

		parent::submitForm($form, $form_state);

		$this->config('remote_entity_creation.authentication')
			->set('domain', $form_state->getValue('domain'))
			->save();
	}

}
