<?php

namespace Drupal\remote_entity_creation\Authentication;

use Drupal\remote_entity_creation\Interfaces\IPAuthenticationInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * @see Drupal\remote_entity_creation\Interfaces\IPAuthenticationInterface
 */
class IPAuthentication implements IPAuthenticationInterface {

	/**
	 * {@inheritdoc}
	 */
	public function setIP(RequestStack $requestStack) {

		$return = $requestStack->getCurrentRequest();
		return $return;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getIPAuthentication() {

		return true;
	}

}
