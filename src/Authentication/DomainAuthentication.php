<?php

namespace Drupal\remote_entity_creation\Authentication;

use Drupal\remote_entity_creation\Interfaces\DomainAuthenticationInterface;
use Drupal\remote_entity_creation\Abstracts\DomainAuthenticationAbstract;
use Symfony\Component\HttpFoundation\Request;

/**
 * @see Drupal\remote_entity_creation\Interfaces\DomainAuthenticationInterface
 */
class DomainAuthentication extends DomainAuthenticationAbstract implements DomainAuthenticationInterface {

	/**
	 * {@inheritdoc}
	 */
	protected $domain = '';

	/**
	 * {@inheritdoc}
	 */
	protected $requestDomain = '';

	/**
	 * {@inheritdoc}
	 */
	public function setDomainName(Request $request) {
		//$previousUrl = \Drupal::request()->server->get('HTTP_REFERER');
		//$return = $request->server->all();

		$config = \Drupal::config('remote_entity_creation.authentication');
		$this->domain = $config->get('domain');

		$this->requestDomain = $request->server->get('HTTP_REFERER');
		$return = $request->server->get('HTTP_REFERER');
		return $return;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getDomainAuthentication() {
		if (trim($this->domain) === trim($this->requestDomain)) {
			return true;
		}

		return false;
	}

}
