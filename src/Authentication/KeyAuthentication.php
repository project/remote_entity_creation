<?php

namespace Drupal\remote_entity_creation\Authentication;

use Drupal\remote_entity_creation\Interfaces\KeyAuthenticationInterface;

/**
 * @see Drupal\remote_entity_creation\Interfaces\KeyAuthenticationInterface
 */
class KeyAuthentication implements KeyAuthenticationInterface {


	/**
	 * @var string 
	 */
	protected $keyRequest = '';

	/**
	 *  Returns authentication key created on install
	 *
	 * @return string
	 */
	protected function viewAuthenticationKey() {

		$key = \Drupal::database()->query('SELECT {special_key} FROM {remote_entity_creation}')->fetchAll();
		return $key[0]->special_key;
	}

	/**
	 * {@inheritdoc}
	 */
	public function setKey($keyString) {

		$this->requestKey = $keyString;
		return $keyString;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getKeyAuthentication() {

		if ($this->viewAuthenticationKey() === trim($this->requestKey)) {
			return true;
		}

		return false;
	}

}
