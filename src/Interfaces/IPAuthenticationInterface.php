<?php

namespace Drupal\remote_entity_creation\Interfaces;

use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Test to see if the domain is the same one set by the
 * administration.
 *
 */
interface IPAuthenticationInterface {

	/**
   * Domain name required
   * @param Object
	 *
   */
  public function setIP(RequestStack $requestStack);

	/**
	 * Returns domain authentication result
	 *
	 *   @return boolean
	 *      false: the domain is not correct
	 *      true:  the domain is correct
	 */
	public function getIPAuthentication();
}
