<?php

namespace Drupal\remote_entity_creation\Interfaces;

/**
 *  Once request data integrity has been tested then entity can be created.
 *
 */
interface DataMappingInterface {

	/**
	 * Returns array with key names relevant to creating a
	 * new entity
	 *
	 * @return array
	 */
	public function getDataMappedArray();
}
