<?php

namespace Drupal\remote_entity_creation\Interfaces;

use Symfony\Component\HttpFoundation\Request;

/**
 * Test to see if the domain is the same one set by the
 * administration.
 *
 */
interface DomainAuthenticationInterface {

	/**
   * Domain name required
   * @param Object
	 *
   */
  public function setDomainName(Request $request);

	/**
	 * Returns domain authentication result
	 *
	 *   @return boolean
	 *      false: the domain is not correct
	 *      true:  the domain is correct
	 */
	public function getDomainAuthentication();
}
