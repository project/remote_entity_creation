<?php

namespace Drupal\remote_entity_creation\Interfaces;

/**
 *  Data integrity from the request received needs to validated and then format
 *  so that an entity can be created
 *
 */
interface RequestDataParsingInterface {


	/**
	 * Returns array with key names relevant to creating a
	 * new entity
	 *
	 * @return array
	 */
	public function getDataArray();
}
