<?php

namespace Drupal\remote_entity_creation\Interfaces;

/**
 *  Once request data integrity has been tested then entity can be created.
 *
 */
interface EntityCreationInterface {


	/**
	 * Returns the result of the entity that has been created the
	 * JSON object needs to provide the following results.
	 *
	 * 1. True or false it was created
	 * 2. Title of the entity
	 * 3. The ID, TID, NID etc
   *
	 * @return Json Object
	 */
	public function getDataCreationResult();
}
