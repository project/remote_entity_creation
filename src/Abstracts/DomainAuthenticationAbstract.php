<?php

namespace Drupal\remote_entity_creation\Abstracts;


/**
 * Test to see if the domain is the same one set by the
 * administration.
 *
 */
abstract class DomainAuthenticationAbstract {

	/**
	 * @var string
	 */
	protected $domain = '';

	/**
	 * @var string
	 */
	protected $requestDomain = '';

}
