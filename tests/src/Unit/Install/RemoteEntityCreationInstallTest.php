<?php

namespace Drupal\Tests\remote_entity_creation\Unit\Install;

use Drupal\Tests\UnitTestCase;
use Drupal\Core\Database\Driver\mysql\Connection;

/**
 *  Testing base RemoteEntityCreation installation
 *
 * @group remote_entity_creation
 */
class RemoteEntityCreationInstallTest extends UnitTestCase {

  /**
   * Testing to see if install test are  is working
   */
  public function testRemoteEntityCreationInstallTest() {
    $expected = 'install';
    $result = 'install';
    $this->assertEquals($expected, $result);
  }

}
