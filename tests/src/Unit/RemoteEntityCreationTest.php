<?php

namespace Drupal\Tests\remote_entity_creation\Unit;

use Drupal\Tests\UnitTestCase;

/**
 *  Testing base RemoteEntityCreation
 *
 * @group remote_entity_creation
 */
class RemoteEntityCreationTest extends UnitTestCase {

  /**
   * Testing base RemoteEntityCreation
   */
  public function testRemoteEntityCreationTest() {
    $expected = 'RemoteEntityCreation';
    $result = 'RemoteEntityCreation';
    $this->assertEquals($expected, $result);
  }

}
