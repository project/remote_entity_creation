<?php

namespace Drupal\Tests\remote_entity_creation\Unit\Authentication;

use Drupal\Tests\UnitTestCase;

use Drupal\MYMODULE\Foo;
use Prophecy\Argument;

/**
 *  Testing base RemoteEntityCreation 
 *  @todo this needs to be test and
 * @group remote_entity_creation
 */
class IPAuthenticationTest extends UnitTestCase {

	/**
   * Testing to see if install test are  is working
   */
    public function testBar() {
		$request = $this->prophesize('\Symfony\Component\HttpFoundation\Request');
		$request->getClientIp()->willReturn('123.123.123.123');
		$request = $request->reveal();

		$request_stack = $this->prophesize('\Symfony\Component\HttpFoundation\RequestStack');
		$request_stack->getCurrentRequest()->willReturn($request);
		$request_stack = $request_stack->reveal();

		$foo = new Foo($request_stack);

		$this->assertEquals($foo->bar(), '123.123.123.123');
	}

}
