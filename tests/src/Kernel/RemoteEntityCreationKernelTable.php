<?php

namespace Drupal\Tests\remote_entity_creation\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Testing RemoteEntityCreationKernelTable base
 *
 * @group remote_entity_creation
 */
class RemoteEntityCreationKernelTable extends KernelTestBase {

  /**
   * Testing to see if table and fields are installed
   */
  public function testTableAndFieldsTest() {
    
    //-- Test  to see if table exists
    $tableList = \Drupal::database()->query('SHOW TABLES')->fetchCol();
    $findTableName = false;
    foreach ($tableList as $tableName) {
      if ($tableName === 'remote_entity_creation') {
				$findTableName = true;
        break;
      }
    }

    $this->assertEquals(true, $findTableName);


    //-- Test for field this relys on if the table exits first.
    $tableFieldNameString = false;
    $findColumnNames = false;
    if ($findTableName !== false) {
       $tableFieldList = \Drupal::database()->query('SELECT {special_key} FROM {remote_entity_creation}')->fetchAll();
			foreach ($tableFieldList as $tablekeyName => $tableFieldName) {
        if ($tablekeyName === 'special_key') {
          if ($tableFieldName !== '') {
            $tableFieldNameString = true;
          }
           $findColumnNames = true;
          break;
        }
      }
    }
    //-- columns names
    $this->assertEquals(true, $findColumnNames);

    //-- if the contain data
    $this->assertEquals(true, $tableFieldNameString);
  }


}
