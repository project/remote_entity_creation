<?php

namespace Drupal\Tests\remote_entity_creation\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Testing RemoteEntityCreationKernel base
 *
 * @group remote_entity_creation
 */
class RemoteEntityCreationKernelTest extends KernelTestBase {

  /**
   * Testing base RemoteEntityCreationKernelTable
   */
  public function testRemoteEntityCreationKernelTest() {
    $expected = 'RemoteEntityCreationKernelTable';
    $result = 'RemoteEntityCreationKernelTable';
    $this->assertEquals($expected, $result);
  }

}
